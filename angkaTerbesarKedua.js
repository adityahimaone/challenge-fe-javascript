const getAngkaTerbesarKedua = (number) => {
  if (typeof number === "undefined") {
    return "Error: Parameter must be filled";
  }
  if (number instanceof Array === false) {
    return "Error: Invalid data type, must be array";
  }
  if (number instanceof Array) {
    let angkaTerbesar = 0;
    let angkaTerbesarKedua = 0;
    for (let i = 0; i < number.length; i++) {
      if (number[i] > angkaTerbesar) {
        angkaTerbesarKedua = angkaTerbesar;
        angkaTerbesar = number[i];
      } else if (number[i] > angkaTerbesarKedua) {
        angkaTerbesarKedua = number[i];
      }
    }
    return angkaTerbesarKedua;
  }
  return "Error: error not found";
};

const dataAngka = [9, 4, 7, 7, 4, 3, 2, 2, 8];

console.log(getAngkaTerbesarKedua(dataAngka));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());
