const changeWord = (selectedText, changedText, text) => {
  const regex = new RegExp(selectedText);
  return text.replace(regex, changedText);
};

const kalimat1 = "Andini sangat mencintai kamu selamanya";
const kalimat2 =
  "Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu";

console.log(changeWord("mencintai", "membenci", kalimat1));
console.log(changeWord("bromo", "sumeru", kalimat2));
