const checkEmail = (email) => {
  if (!email) {
    return "Error: Parameter must be filled";
  }
  if (typeof email !== "string") {
    return "Error: Invalid data type, must be string";
  }
  if (email.search(/@|\./) === -1) {
    return "Error: Invalid email, email must have @ or . character";
  }
  const emailPattern = /^[a-z0-9]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
  const isValid = emailPattern.test(email);
  return isValid ? "VALID" : "INVALID";
};

console.log(checkEmail("apranata@binar.co.id"));
console.log(checkEmail("apranata@binar.com"));
console.log(checkEmail("apranata@binar"));
console.log(checkEmail("apranata"));
console.log(checkEmail(3322));
console.log(checkEmail());
