const dataPenjualanPakAldi = [
  {
    nameProduct: "Sepatu Futsal Nike Valpor Academy 8",
    hargaSatuan: 760000,
    kategori: "Sepatu Sport",
    totalPenjualan: 90,
  },
  {
    nameProduct: "Sepatu Warrior Tristan Black Brown High - Original",
    hargaSatuan: 960000,
    kategori: "Sepatu Sneaker",
    totalPenjualan: 37,
  },
  {
    nameProduct: "Sepatu Warrior Tristan Maroon High - Original",
    hargaSatuan: 360000,
    kategori: "Sepatu Sneaker",
    totalPenjualan: 90,
  },
  {
    nameProduct: "Sepatu Futsal Nike Valpor Academy 8",
    hargaSatuan: 120000,
    kategori: "Sepatu Sneaker",
    totalPenjualan: 90,
  },
];

const getTotalPenjualan = (dataPenjualan) => {
  let totalPenjualan = 0;
  if (dataPenjualan instanceof Object === false) {
    return "Error: Invalid data type, must be object";
  } else {
    dataPenjualan?.forEach((item) => {
      totalPenjualan += item.totalPenjualan;
    });
  }
  return totalPenjualan;
};

console.log(getTotalPenjualan(dataPenjualanPakAldi));
