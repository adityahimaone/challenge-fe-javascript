const isValidPassword = (givenPassword) => {
  if (typeof givenPassword === "undefined") {
    return "Error: Parameter must be filled";
  }
  if (typeof givenPassword !== "string") {
    return "Error: Invalid data type, must be string";
  }
  const passwordPattern = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.{8,})/;
  const isValid = passwordPattern.test(givenPassword);
  return isValid ? true : false;
};

console.log(isValidPassword("Meong2021"));
console.log(isValidPassword("meong2021"));
console.log(isValidPassword("MEONG2021"));
console.log(isValidPassword("@eong"));
console.log(isValidPassword(0));
console.log(isValidPassword());
