const dataPenjualanNovel = [
  {
    idProduct: "BOOK002421",
    namaProduk: "Pulang - Pergi",
    penulis: "Tere Liye",
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStok: 17,
  },
  {
    idProduct: "BOOK002351",
    namaProduk: "Selamat Tinggal",
    penulis: "Tere Liye",
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStok: 20,
  },
  {
    idProduct: "BOOK002941",
    namaProduk: "Garis Waktu",
    penulis: "Fiersa Besari",
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStok: 5,
  },
  {
    idProduct: "BOOK002941",
    namaProduk: "laskar Pelangi",
    penulis: "Andrea Hirata",
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStok: 56,
  },
];

const convertToIDR = (number) => {
  return new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    maximumFractionDigits: 0,
  }).format(number);
};

const getInfoPenjualan = (dataPenjualanNovel) => {
  let result = {
    totalKeuntungan: 0,
    totalModal: 0,
    persentaseKeuntungan: 0,
    produkBukuTerlaris: "",
    penulisTerlaris: "",
  };

  dataPenjualanNovel.forEach((item) => {
    result.totalKeuntungan +=
      (item.hargaJual - item.hargaBeli) * item.totalTerjual;

    result.totalModal += (item.totalTerjual + item.sisaStok) * item.hargaBeli;

    result.persentaseKeuntungan =
      ((result.totalKeuntungan / result.totalModal) * 100).toFixed(0) + "%";
  });

  const bukuTerlaris = dataPenjualanNovel.reduce((prev, current) =>
    prev.totalTerjual > current.totalTerjual ? prev : current
  );

  result.produkBukuTerlaris = bukuTerlaris.namaProduk;

  const penulis = dataPenjualanNovel.reduce((prev, current) => {
    const data = { ...prev };
    if (data[current.penulis]) {
      data[current.penulis] += current.totalTerjual;
    } else {
      data[current.penulis] = current.totalTerjual;
    }
    return data;
  }, {});

  const penulisTerlaris = Object.keys(penulis).reduce((prev, current) =>
    penulis[prev] > penulis[current] ? prev : current
  );
  result.penulisTerlaris = penulisTerlaris;

  result.totalKeuntungan = convertToIDR(result.totalKeuntungan);

  result.totalModal = convertToIDR(result.totalModal);

  return result;
};

console.log(getInfoPenjualan(dataPenjualanNovel));
