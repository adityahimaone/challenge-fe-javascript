const getSplitName = (personalName) => {
  if (typeof personalName !== "string") {
    return "Error : This function only for type string";
  } else {
    const name = personalName.split(" ");
    return name.length === 3
      ? {
          firstName: name[0],
          middleName: name[1],
          lastName: name[2],
        }
      : name.length === 2
      ? {
          firstName: name[0],
          middleName: null,
          lastName: name[name.length - 1],
        }
      : name.length === 1
      ? {
          firstName: name[0],
          middleName: null,
          lastName: null,
        }
      : "Error : This function is only for 3 characters name";
  }
};

console.log(getSplitName("Aldi Daniela Pranata"));
console.log(getSplitName("Dwi Kuncoro"));
console.log(getSplitName("Aurora"));
console.log(getSplitName("Aurora Aureliya Sukma Darma"));
console.log(getSplitName(0));
